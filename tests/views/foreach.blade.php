@function(mylist ($items))
	<ul>
		@foreach($items as $item)
			<li>{{$item * 2}}</li>
		@endforeach
	</ul>
@endfunction

@mylist([1, 2, 3])