@function(hello ($who) use ($lang))
	{{$lang == 'de' ? 'Hallo' : 'Hello'}} {{$who}}!
@endfunction

<?php $lang = 'en'; ?>
<?php hello('World', $lang, $__env); ?>