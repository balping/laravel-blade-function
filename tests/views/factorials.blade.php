@function(factorials ($n))
	@if($n <= 1)
		{{1}}
		@return(1)
	@endif

	{{$tmp = $n * factorials($n - 1, $__env)}}

	@return($tmp)
@endfunction

@factorials(5)