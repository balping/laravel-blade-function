<?php

namespace Balping\BladeFunction\Tests;

use Illuminate\Support\ServiceProvider;

class BladeFunctionTestServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
       $this->loadViewsFrom(__DIR__.'/views', 'test');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
