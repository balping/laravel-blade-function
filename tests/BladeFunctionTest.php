<?php


namespace Balping\BladeFunction\Tests;
use PHPUnit\Framework\Attributes\Test;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Testing\Concerns\InteractsWithViews;
use Illuminate\Foundation\Testing\Concerns\InteractsWithExceptionHandling;


class BladeFunctionTest extends \Orchestra\Testbench\TestCase {
	use InteractsWithViews;
	use InteractsWithExceptionHandling;

	public function setUp() : void {
		parent::setUp();
	}

	protected function getPackageProviders($app) {
		return [
			\Balping\BladeFunction\BladeFunctionServiceProvider::class,
			\Balping\BladeFunction\Tests\BladeFunctionTestServiceProvider::class,
		];
	}

	#[Test]
	public function hello_world(){
		Route::view('hello', 'test::hello');

		$response = $this->get('hello');
		$response->assertSee("Hello World");
	}

	#[Test]
	public function parentheses_are_optional(){
		Route::view('with-parentheses', 'test::with_parentheses');
		Route::view('without-parentheses', 'test::without_parentheses');

		$response = $this->get('with-parentheses');
		$response->assertSeeInOrder(["Hello", "Hello"]);

		$response = $this->get('without-parentheses');
		$response->assertSeeInOrder(["Hello", "Hello"]);
	}

	#[Test]
	public function blade_directive_inside_function(){
		Route::view('foreach', 'test::foreach');

		$response = $this->get('foreach');
		$response->assertSeeInOrder(["2", "4", "6"]);
	}

	#[Test]
	public function return_directive(){
		Route::view('return', 'test::return');

		$response = $this->get('return');
		$response->assertSee("42");
	}

	#[Test]
	public function variable_from_parent_scope(){
		Route::view('use', 'test::use');

		$response = $this->get('use');
		$response->assertSee("Hello World");
	}

	#[Test]
	public function call_from_php(){
		Route::view('call-from-php', 'test::call_from_php');

		$response = $this->get('call-from-php');
		$response->assertSee("Hello World");
	}

	#[Test]
	public function call_from_php_use(){
		Route::view('call-from-php-use', 'test::call_from_php_use');

		$response = $this->get('call-from-php-use');
		$response->assertSee("Hello World");
	}

	#[Test]
	public function recursion(){
		Route::view('recursion', 'test::factorials');

		$response = $this->get('recursion');
		$response->assertSeeInOrder([1, 2, 6, 24, 120]);
	}

	#[Test]
	public function exception_on_invalid_fuction(){
		Route::view('invalid', 'test::invalid_function');

		$response = $this->get('invalid');
		$response->assertStatus(500);
	}
}